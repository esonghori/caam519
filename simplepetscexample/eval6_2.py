#!/usr/bin/python

import os

sizes = {}
flops = {}
pc_types = ['lu','gamg']
for pc_type in pc_types:
  sizes[pc_type] = []
  flops[pc_type] = []

N = 5
for pc_type in pc_types:
  for k in range(5):
    Nx = (1+2**(k+N-1))
    modname = 'perf_ngmres%s%d' % (pc_type, k)
    options = [
      '-da_grid_x', str(N), 
      '-da_grid_y', str(N), 
      '-snes_type', 'newtonls', 
      '-snes_grid_sequence', str(k+1), 
      '-da_refine', '1',
      '-pc_type', pc_type, 
      '-ksp_rtol', '1e-9',
      '-mms', '11',
      '-log_view', ':%s.py:ascii_info_detail' % modname
      ]
    os.system('./bin/ex5 '+' '.join(options))
    perfmod = __import__(modname)
    sizes[pc_type].append(Nx ** 2)
    flops[pc_type].append(perfmod.Stages['Main Stage']['SNESSolve'][0]['flops'])
  print zip(sizes[pc_type], flops[pc_type])

from pylab import legend, plot, loglog, show, title, xlabel, ylabel
size = sizes[pc_types[0]]
flop = zip(flops[pc_types[0]], flops[pc_types[1]])
loglog(size, flop)
title('%s for MMS $\sin(\pi x)\sin(\pi y)$' % pc_types)
xlabel('Problem Size $N$')
ylabel('Flops')
legend((pc_types[0], pc_types[1]))
show()
