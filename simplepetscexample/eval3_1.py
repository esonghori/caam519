#!/usr/bin/python

import os

sizes = {}
times = {}
snes_types = ['shell', 'newtonls']
for snes_type in snes_types:
  sizes[snes_type] = []
  times[snes_type] = []

for snes_type in snes_types:
  for k in range(5):
    Nx = 10 * 2 ** k
    modname = 'perf_%s%d' % (snes_type, k)
    options = [
      '-da_grid_x', str(Nx), 
      '-da_grid_y', str(Nx), 
      '-snes_type', snes_type, 
      '-pc_type', 'mg', 
      '-snes_max_it', '100000', 
      '-snes_atol', '1e-7', 
      '-snes_rtol', '1e-7',
      '-log_view', ':%s.py:ascii_info_detail' % modname]
    os.system('./bin/ex5 '+' '.join(options))
    perfmod = __import__(modname)
    sizes[snes_type].append(Nx ** 2)
    times[snes_type].append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
  print zip(sizes[snes_type], times[snes_type])

from pylab import legend, plot, loglog, show, title, xlabel, ylabel
size = sizes[snes_types[0]]
time = zip(times[snes_types[0]], times[snes_types[1]])
loglog(time, size)
title('%s %s ex5' % (snes_types[0], snes_types[1]))
ylabel('Precision (Problem Size $N$)')
xlabel('Work (Time (s))')
legend((snes_types[0], snes_types[1]))
show()
