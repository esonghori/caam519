#!/usr/bin/python

import numpy as np
from pylab import legend, loglog, show, title, xlabel, ylabel


N = np.array([169, 625, 2401, 9409, 37249, 148225]);

#MMS x(1-x)y(1-y)
#N: 169 error l2 2.20735e-06 inf 7.9559e-05
#N: 625 error l2 3.081e-07 inf 2.42794e-05
#N: 2401 error l2 4.0427e-08 inf 8.29001e-06
#N: 9409 error l2 5.16863e-09 inf 2.62871e-06
#N: 37249 error l2 6.53061e-10 inf 7.96102e-07
#N: 148225 error l2 8.20737e-11 inf 2.33813e-07

#el2 = np.array([2.20735e-06, 3.081e-07, 4.0427e-08, 5.16863e-09, 6.53061e-10, 8.20737e-11])
#einf = np.array([7.9559e-05, 2.42794e-05, 8.29001e-06, 2.62871e-06, 7.96102e-07, 2.33813e-07])
#title('SNES ex5 MMS $u = x(1-x)y(1-y)$')

#MMS \sin(\pi x)\sin(\pi y)
#N: 169 error l2 0.00030817 inf 0.0169964
#N: 625 error l2 3.88389e-05 inf 0.0039253
#N: 2401 error l2 4.97907e-06 inf 0.0010981
#N: 9409 error l2 6.33007e-07 inf 0.000367842
#N: 37249 error l2 7.98778e-08 inf 0.000115731
#N: 148225 error l2 1.00303e-08 inf 3.49045e-05


el2 = np.array([0.00030817, 3.88389e-05, 4.97907e-06, 6.33007e-07, 7.98778e-08, 1.00303e-08])
einf = np.array([0.0169964, 0.0039253, 0.0010981, 0.000367842, 0.000115731, 3.49045e-05])
title('SNES ex5 MMS $u = \sin(\pi x)\sin(\pi y)$')

loglog(N, el2, 'r', N, (0.0002/169**-1.5)*N** -1.5, 'r--',
N, einf, 'g', N, (0.01*169) * 1/N, 'g--')
xlabel('Number of Dof $N$')
ylabel('Solution Error $e$')
legend(['$\ell 2$', '$h^{-3} = N^{-3/2}$',
'$\ell \infty$', '$h^{-2} = N^{-1}$'], 'upper right')

show()