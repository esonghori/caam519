#!/usr/bin/python

import os

sizes = {}
times = {}
pc_types = ['gamg', 'ilu']
for pc_type in pc_types:
  sizes[pc_type] = []
  times[pc_type] = []

for pc_type in pc_types:
  for k in range(5):
    Nx = 10 * 2 ** k
    modname = 'perf_ngmres%s%d' % (pc_type, k)
    options = ['-da_grid_x', str(Nx), '-da_grid_y', str(Nx), '-snes_type',
      'ngmres', 'pc_type', pc_type, '-log_view',
      ':%s.py:ascii_info_detail' % modname]
    os.system('./bin/ex5 '+' '.join(options))
    perfmod = __import__(modname)
    sizes[pc_type].append(Nx ** 2)
    times[pc_type].append(perfmod.Stages['Main Stage']['SNESSolve'][0]['time'])
  print zip(sizes[pc_type], times[pc_type])

from pylab import legend, plot, loglog, show, title, xlabel, ylabel
size = sizes[pc_types[0]]
time = zip(times[pc_types[0]], times[pc_types[1]])
loglog(size, time)
title('%s ex5' % pc_types)
xlabel('Problem Size $N$')
ylabel('Time (s)')
legend((pc_types[0], pc_types[1]))
show()
