#include "petscdmda.h"
#include "petscsnes.h"
/* 
   User-defined application context - contains data needed by the 
   application-provided call-back routines, FormJacobianLocal() and
   FormFunctionLocal().
*/
typedef struct {
  PetscReal     param;             /* test problem parameter */
  PetscLogEvent initialGuessEvent; /* event for custom initialization */
} AppCtx;

/*
   User-defined routines
*/
extern PetscErrorCode MyInitialGuess(DM,AppCtx*,Vec);
extern PetscErrorCode FormInitialGuess(DM,AppCtx*,Vec);
extern PetscErrorCode FormFunctionLocal(DMDALocalInfo*,PetscScalar**,PetscScalar**,AppCtx*);
extern PetscErrorCode FormExactSolution1(DM,AppCtx*,Vec);
extern PetscErrorCode FormFunctionLocalMMS1(DMDALocalInfo*,PetscScalar**,PetscScalar**,AppCtx*);
extern PetscErrorCode FormExactSolution2(DM,AppCtx*,Vec);
extern PetscErrorCode FormFunctionLocalMMS2(DMDALocalInfo*,PetscScalar**,PetscScalar**,AppCtx*);
extern PetscErrorCode FormFunctionLocalMMS11(DMDALocalInfo*,PetscScalar**,PetscScalar**,AppCtx*);
extern PetscErrorCode FormFunctionLocalMMS22(DMDALocalInfo*,PetscScalar**,PetscScalar**,AppCtx*);
extern PetscErrorCode FormJacobianLocal(DMDALocalInfo*,PetscScalar**,Mat,Mat,AppCtx*);
extern PetscErrorCode FormObjectiveLocal(DMDALocalInfo*,PetscScalar**,PetscReal*,AppCtx*);
#if defined(PETSC_HAVE_MATLAB_ENGINE)
extern PetscErrorCode FormFunctionMatlab(SNES,Vec,Vec,void*);
#endif
extern PetscErrorCode NonlinearGS(SNES,Vec,Vec,void*);
extern PetscErrorCode SimpleMixing(SNES,Vec);