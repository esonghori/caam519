#include "ex5.h"

#undef __FUNCT__
#define __FUNCT__ "MyInitialGuess"
/*@ 
   MyInitialGuess - Forms initial approximation usinf coordinate information.

   Input Parameters:
   user - user-defined application context
   X - vector

   Output Parameter:
   X - vector
@*/
PetscErrorCode MyInitialGuess(DM da, AppCtx *user, Vec X) {
  DM             cda;
  Vec            c;
  PetscScalar  **x;
  DMDACoor2d   **coords;
  PetscInt       i,j,Mx,My,xs,ys,xm,ym;
  PetscReal      lambda,temp1;
  PetscErrorCode ierr;

  PetscFunctionBegin;
  ierr = PetscLogEventBegin(user->initialGuessEvent,0,0,0,0);CHKERRQ(ierr);
  ierr = DMDAGetInfo(da,PETSC_IGNORE,&Mx,&My,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,
                     PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE,PETSC_IGNORE);

  lambda = user->param;
  temp1  = lambda/(lambda + 1.0);

  ierr = DMGetCoordinateDM(da,&cda);CHKERRQ(ierr);
  ierr = DMGetCoordinatesLocal(da,&c);CHKERRQ(ierr);
  ierr = DMDAGetCorners(da,&xs,&ys,PETSC_NULL,&xm,&ym,PETSC_NULL);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(da,X,&x);CHKERRQ(ierr);
  ierr = DMDAVecGetArray(cda,c,&coords);CHKERRQ(ierr);
  for (j=ys; j<ys+ym; j++) {
    for (i=xs; i<xs+xm; i++) {
      if (i == 0 || j == 0 || i == Mx-1 || j == My-1) {
        /* boundary conditions are all zero Dirichlet */
        x[j][i] = 0.0; 
      } else {
        x[j][i] = temp1*sqrt(2.0*PetscMin(coords[j][i+1].x + coords[j][i-1].x, coords[j+1][i].y + coords[j-1][i].y)); 
      }
    }
  }
  ierr = PetscLogFlops(5*ym*xm + 6);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(cda,c,&coords);CHKERRQ(ierr);
  ierr = DMDAVecRestoreArray(da,X,&x);CHKERRQ(ierr);
  ierr = PetscLogEventEnd(user->initialGuessEvent,0,0,0,0);CHKERRQ(ierr);
  PetscFunctionReturn(0);
}
